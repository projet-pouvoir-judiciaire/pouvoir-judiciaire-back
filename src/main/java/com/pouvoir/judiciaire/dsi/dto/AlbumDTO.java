/**
 * 
 */
package com.pouvoir.judiciaire.dsi.dto;

/**
 * @author Veronique DIBI
 */
public class AlbumDTO {
	
	private String title;
	private String gender;
	private long totalLengthInSeconds;
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public long getTotalLengthInSeconds() {
		return totalLengthInSeconds;
	}
	public void setTotalLengthInSeconds(long totalLengthInSeconds) {
		this.totalLengthInSeconds = totalLengthInSeconds;
	}
	
	
}
