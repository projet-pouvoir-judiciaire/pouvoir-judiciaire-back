/**
 * 
 */
package com.pouvoir.judiciaire.dsi.dto;

import java.util.List;

/**
 * @author Veronique DIBI
 */
public class ArtistDTO {
	
	private int artistId;
	private String name;
	
	private List<AlbumDTO> albums;
	
	
	public int getArtistId() {
		return artistId;
	}
	public void setArtistId(int artistId) {
		this.artistId = artistId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<AlbumDTO> getAlbums() {
		return albums;
	}
	public void setAlbums(List<AlbumDTO> albums) {
		this.albums = albums;
	}
	
	
}
