/**
 * 
 */
package com.pouvoir.judiciaire.dsi.dto;

import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Veronique DIBI
 */
public class StatisticsDTO {
	
	private static final int BEST_YEAR_COUNT_THESROLD = 3;
	private ArtistDTO artistsDTO;
	@JsonIgnore
	private Map<Integer, Integer> yearTrackCountSold;
	private int bestYearCount;
	
	
	public ArtistDTO getArtistsDTO() {
		return artistsDTO;
	}
	public void setArtistsDTO(ArtistDTO artistsDTO) {
		this.artistsDTO = artistsDTO;
	}
	public Map<Integer, Integer> getYearTrackCountSold() {
		return yearTrackCountSold;
	}
	public void setYearTrackCountSold(Map<Integer, Integer> yearTrackCountSold) {
		this.yearTrackCountSold = yearTrackCountSold;
	}
	public int getBestYearCount() {
		return bestYearCount;
	}
	public void setBestYearCount(int bestYearCount) {
		this.bestYearCount = bestYearCount;
	}
	
	public void setBestYearCount() {
		Queue<Map.Entry<Integer, Integer>> queue = new ArrayBlockingQueue<>(yearTrackCountSold.size());
		yearTrackCountSold.entrySet().forEach(queue::add);
		
		while(queue.peek() != null && queue.peek().getValue() < BEST_YEAR_COUNT_THESROLD) {
			queue.poll(); // let's remove the head of this queue
		}
		
		List<Set<Integer>> yearSet = new ArrayList<>();
		
		Set<Integer> current = new HashSet<>();
		Integer previousYear = null;
		
		for(Map.Entry<Integer, Integer> entry : queue) {
			if(entry.getValue() >= BEST_YEAR_COUNT_THESROLD) {
				if(previousYear != null && entry.getKey() - previousYear != 1) {
					yearSet.add(new HashSet<>(current));
					current = new HashSet<>();
				}
				current.add(entry.getKey());
			} 
			
			previousYear = entry.getKey();
		}
		
		if(!current.isEmpty()) {
			yearSet.add(new HashSet<>(current));
		}
		
		int result = yearSet.stream().mapToInt(s -> s.size()).max().orElse(0);
		bestYearCount = result;
	}	
}