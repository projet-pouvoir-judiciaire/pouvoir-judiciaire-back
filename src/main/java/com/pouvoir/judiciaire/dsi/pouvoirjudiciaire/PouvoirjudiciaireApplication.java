package com.pouvoir.judiciaire.dsi.pouvoirjudiciaire;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author Veronique DIBI
 */
@ComponentScan(basePackages= {"com.pouvoir.judiciaire.dsi"})
@EnableJpaRepositories("com.pouvoir.judiciaire.dsi")
@EntityScan("com.pouvoir.judiciaire.dsi")
@SpringBootApplication
public class PouvoirjudiciaireApplication {
	

	public static void main(String[] args) {
		SpringApplication.run(PouvoirjudiciaireApplication.class, args);
	}
}