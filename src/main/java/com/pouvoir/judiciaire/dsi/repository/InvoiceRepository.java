/**
 * 
 */
package com.pouvoir.judiciaire.dsi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pouvoir.judiciaire.dsi.entities.Invoice;

/**
 * @author Veronique DIBI
 */

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Integer> {

	@Query("select i from Invoice i left join fetch i.invoiceLines ils left join fetch ils.track t left join fetch t.album a left join fetch a.artist ar")
	List<Invoice> findAllInvoicesForStatistics();

	
}
