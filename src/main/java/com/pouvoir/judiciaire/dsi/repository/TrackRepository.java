/**
 * 
 */
package com.pouvoir.judiciaire.dsi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pouvoir.judiciaire.dsi.entities.Track;

/**
 * 
 */

@Repository
public interface TrackRepository extends JpaRepository<Track, Integer> {
	
	@Query("select tr from Track tr left join fetch tr.genre gr left join fetch tr.album al left join fetch al.artist a")
	List<Track> findAllTracksForAlbums();

	@Query("select tr from Track tr left join fetch tr.genre gr left join fetch tr.album al left join fetch al.artist a where a.artistId = :artistId")
	List<Track> findAllTracksForAlbumsByArtistId(@Param("artistId") Integer artistId);

	
}
