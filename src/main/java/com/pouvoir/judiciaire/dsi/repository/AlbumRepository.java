/**
 * 
 */
package com.pouvoir.judiciaire.dsi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pouvoir.judiciaire.dsi.entities.Album;


/**
 * @author Veronique DIBI
 */
@Repository
public interface AlbumRepository extends JpaRepository<Album, Integer> {

	
}
