/**
 * 
 */
package com.pouvoir.judiciaire.dsi.controller;


import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pouvoir.judiciaire.dsi.dto.StatisticsDTO;
import com.pouvoir.judiciaire.dsi.service.InvoiceService;

/**
 * @author Veronique DIBI
 */



@RestController
@RequestMapping(path = "/ws")
public class InvoiceController {
	
	
	private final InvoiceService invoiceService;
	
	
	
	public InvoiceController(InvoiceService invoiceService){
	  this.invoiceService = invoiceService;
	}
    
	
    @GetMapping(path = "/invoice/artists/statistics")
    public List<StatisticsDTO> getAllArtistStatistics() {
    	return this.invoiceService.getAllArtistStatistics();
    }   
}