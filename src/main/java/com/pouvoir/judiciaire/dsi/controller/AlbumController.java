/**
 * 
 */
package com.pouvoir.judiciaire.dsi.controller;


import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pouvoir.judiciaire.dsi.entities.Album;
import com.pouvoir.judiciaire.dsi.service.AlbumService;

/**
 * @author Veronique DIBI
 */



@RestController
@RequestMapping(path = "/ws")
public class AlbumController {
	
	
	private final AlbumService albumService;
	
	
	
	public AlbumController( AlbumService albumService){
	  this.albumService = albumService;
	}
    
	
    @GetMapping(path = "/album/{albumId}")
    Album getAlbumById(@PathVariable final Integer albumId) {
    	return this.albumService.getAlbumById(albumId);
 
    }
    
    @GetMapping(path = "/allAlbums")
    List<Album> getAlbum(){
		return this.albumService.getAlbum();
		
	}
	
}
