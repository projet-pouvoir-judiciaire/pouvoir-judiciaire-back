/**
 * 
 */
package com.pouvoir.judiciaire.dsi.controller;


import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pouvoir.judiciaire.dsi.dto.ArtistDTO;
import com.pouvoir.judiciaire.dsi.service.ArtistService;

/**
 * @author Veronique DIBI
 */



@RestController
@RequestMapping(path = "/ws")
public class ArtistController {
	
	
	private final ArtistService artistService;
	
	
	
	public ArtistController(ArtistService artistService){
	  this.artistService = artistService;
	}
    
	
    @GetMapping(path = "/artist/{artistId}")
    public ArtistDTO getArtistById(@PathVariable final Integer artistId) {
    	return this.artistService.getArtistById(artistId);
    }
    
    @GetMapping(path = "/artists")
    public List<ArtistDTO> getArtists(){
		return this.artistService.getAllArtists();
		
	}
    
    @GetMapping(path = "/artists-with-albums")
    public List<ArtistDTO> getArtistsAndAlbums(){
		return this.artistService.getAllArtistsAndAlbums();
		
	}
    
    @GetMapping(path = "/artist-with-albums/{artistId}")
    public ArtistDTO getArtistAndAlbums(@PathVariable final Integer artistId){
		return this.artistService.getArtistAndAlbumsByArtistId(artistId);
		
	}
    
	
}
