package com.pouvoir.judiciaire.dsi.entities;

import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
/**
 * @author Veronique DIBI
 */
@Entity
@Table(name = "track")
public class Track {
	@Id
	@Column(name = "trackid")
	private int trackId;
	
	@Column(name = "name")
	private int name;
	
	
	@Column(name = "composer")
	private String composer;
	
	@Column(name = "milliseconds")
	private int milliseconds;
	
	@Column(name = "bytes")
	private int bytes;
	
	@Column(name = "unitprice")
	private float unitPrice;
	
	@ManyToOne(targetEntity = Album.class)
	@JoinColumn(name="albumid")
	private Album album;
	
	@OneToMany(targetEntity = InvoiceLine.class)
	private Set<InvoiceLine> invoiceLines;
	
	
	@ManyToOne(targetEntity = Genre.class)
	@JoinColumn(name="genreid")
	private Genre genre;
	
	@ManyToOne(targetEntity = MediaType.class)
	@JoinColumn(name="mediatypeid")
	private MediaType mediaType;
	

	public int getTrackId() {
		return trackId;
	}

	public void setTrackId(int trackId) {
		this.trackId = trackId;
	}

	public int getName() {
		return name;
	}

	public void setName(int name) {
		this.name = name;
	}


	public String getComposer() {
		return composer;
	}

	public void setComposer(String composer) {
		this.composer = composer;
	}

	public Integer getMilliseconds() {
		return milliseconds;
	}

	public void setMilliseconds(int milliseconds) {
		this.milliseconds = milliseconds;
	}

	public int getBytes() {
		return bytes;
	}

	public void setBytes(int bytes) {
		this.bytes = bytes;
	}

	public float getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(float unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public Set<InvoiceLine> getInvoiceLines() {
		return invoiceLines;
	}

	public void setInvoiceLines(Set<InvoiceLine> invoiceLines) {
		this.invoiceLines = invoiceLines;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public MediaType getMediaType() {
		return mediaType;
	}

	public void setMediaType(MediaType mediaType) {
		this.mediaType = mediaType;
	}
	
	
	
}