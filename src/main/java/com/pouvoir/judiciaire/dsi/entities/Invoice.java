package com.pouvoir.judiciaire.dsi.entities;

import java.time.LocalDateTime;
import java.util.Set;

import com.pouvoir.judiciaire.dsi.mapper.MyLocalDateTimeConverter;

import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
/**
 * @author Veronique DIBI
 */
@Entity
@Table(name = "invoice")
public class Invoice {

	@Id
	@Column(name = "invoiceid")
	private int invoiceId;
	
	@Column(name = "customerid")
	private int customerId;
	
	@Column(name = "invoicedate")
	@Convert(converter = MyLocalDateTimeConverter.class)
	private LocalDateTime invoiceDate;
	
	@Column(name = "billingaddress")
	private String billingAddress;
	
	@Column(name = "billingcity")
	private String billingCity;
	
	@Column(name = "billingstate")
	private String billingState;
	
	@Column(name = "billingcountry")
	private String billingCountry;
	
	@Column(name = "billingpostalcode")
	private String billingPostalCode;
	
	@OneToMany
	@JoinColumn(name="invoicelineid")
	private Set<InvoiceLine> invoiceLines;	

	public int getInvoiceId() {
		return invoiceId;
	}


	public void setInvoiceId(int invoiceId) {
		this.invoiceId = invoiceId;
	}



	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public LocalDateTime getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(LocalDateTime invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getBillingCity() {
		return billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	public String getBillingState() {
		return billingState;
	}

	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}

	public String getBillingCountry() {
		return billingCountry;
	}

	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}

	public String getBillingPostalCode() {
		return billingPostalCode;
	}

	public void setBillingPostalCode(String billingPostalCode) {
		this.billingPostalCode = billingPostalCode;
	}


	public Set<InvoiceLine> getInvoiceLines() {
		return invoiceLines;
	}


	public void setInvoiceLines(Set<InvoiceLine> invoiceLines) {
		this.invoiceLines = invoiceLines;
	}

	
}