package com.pouvoir.judiciaire.dsi.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
/**
 * @author Veronique DIBI
 */
@Entity
@Table(name = "genre")
public class Genre {

	@Id
	@Column(name = "genreid")
	public int genreId;
	
	@Column(name = "name")
	public String name;

	public int getGenreId() {
		return genreId;
	}

	public void setGenreId(int genreId) {
		this.genreId = genreId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
}
