package com.pouvoir.judiciaire.dsi.entities;

import java.util.Objects;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

/**
 * @author Veronique DIBI
 */
@Entity
@Table(name = "artist")

public class Artist {
	
	@Id
	@Column(name = "artistid")
	private int artistId;
	
	@Column(name = "name")
	private String name;
	
	@OneToMany
	@JoinColumn(name = "albumid")
	private Set<Album> albums;	

	public Artist() {}

	public int getArtistId() {
		return artistId;
	}


	public void setArtistId(int artistId) {
		this.artistId = artistId;
	}




	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}




	public Set<Album> getAlbums() {
		return albums;
	}




	public void setAlbums(Set<Album> albums) {
		this.albums = albums;
	}

	@Override
	public int hashCode() {
		return Objects.hash(artistId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Artist other = (Artist) obj;
		return artistId == other.artistId;
	}
	
	
	
}

