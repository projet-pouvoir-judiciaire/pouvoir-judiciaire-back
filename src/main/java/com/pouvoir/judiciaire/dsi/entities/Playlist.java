package com.pouvoir.judiciaire.dsi.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
/**
 * @author Veronique DIBI
 */
@Entity
@Table(name = "playlist")
public class Playlist {
	
	@Id
	@Column(name = "playlistid")
	private int playlistId;
	
	@Column(name = "name")
	private String name;

	
	
	public int getPlaylistId() {
		return playlistId;
	}

	public void setPlaylistId(int playlistId) {
		this.playlistId = playlistId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}