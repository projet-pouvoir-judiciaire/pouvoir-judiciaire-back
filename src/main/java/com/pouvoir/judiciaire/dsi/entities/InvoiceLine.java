package com.pouvoir.judiciaire.dsi.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
/**
 * @author Veronique DIBI
 */
@Entity
@Table(name = "invoiceline")
public class InvoiceLine {

	@Id
	@Column(name = "invoicelineid")
	private int invoiceLineId;
	
	
	@Column(name = "unitprice")
	private float unitPrice;
	
	@Column(name = "quantity")
	private float quantity;
	
	@ManyToOne(targetEntity = Track.class)
	@JoinColumn(name="trackid")
	private Track track;
	
	@ManyToOne(targetEntity = Invoice.class)
	@JoinColumn(name="invoiceid")
	private Invoice invoice;
	


	public int getInvoiceLineId() {
		return invoiceLineId;
	}

	public void setInvoiceLineId(int invoiceLineId) {
		this.invoiceLineId = invoiceLineId;
	}


	public Track getTrack() {
		return track;
	}

	public void setTrack(Track track) {
		this.track = track;
	}

	public float getUnitPrivce() {
		return unitPrice;
	}

	public void setUnitPrivce(float unitPrivce) {
		this.unitPrice = unitPrivce;
	}

	public float getQuantity() {
		return quantity;
	}

	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}
}