package com.pouvoir.judiciaire.dsi.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
/**
 * @author Veronique DIBI
 */
@Entity
@Table(name = "mediatype")
public class MediaType {
	

	@Id
	@Column(name = "mediatypeid")
	private int mediaTypeId;
	
	@Column(name = "name")
	private String name;

	public int getMediaTypeId() {
		return mediaTypeId;
	}

	public void setMediaTypeId(int mediaTypeId) {
		this.mediaTypeId = mediaTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
