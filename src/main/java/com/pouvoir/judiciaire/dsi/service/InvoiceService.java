/**
 * 
 */
package com.pouvoir.judiciaire.dsi.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pouvoir.judiciaire.dsi.dto.StatisticsDTO;
import com.pouvoir.judiciaire.dsi.entities.Album;
import com.pouvoir.judiciaire.dsi.entities.Artist;
import com.pouvoir.judiciaire.dsi.entities.Invoice;
import com.pouvoir.judiciaire.dsi.entities.InvoiceLine;
import com.pouvoir.judiciaire.dsi.entities.Track;
import com.pouvoir.judiciaire.dsi.mapper.ArtistMapper;
import com.pouvoir.judiciaire.dsi.repository.InvoiceRepository;

/**
 *  @author Véronque DIBI
 */
@Service
public class InvoiceService {
	
	private final InvoiceRepository invoiceRepository;

	/**
	 * 
	 */
	public InvoiceService(InvoiceRepository invoiceRepository) {
		this.invoiceRepository = invoiceRepository;
	}

	@Transactional
	public List<StatisticsDTO> getAllArtistStatistics() {
		List<Invoice> invoices = invoiceRepository.findAllInvoicesForStatistics();
		//let's sort by invoiceDate
		invoices = invoices.stream().sorted((a, b) -> a.getInvoiceDate().compareTo(b.getInvoiceDate())).toList();
		Map<Integer, List<Artist>> artistMap = new HashMap<>();
		
		invoices.forEach(invoice -> {
			int year = invoice.getInvoiceDate().getYear();
			List<Artist> extractedArtits = invoice.getInvoiceLines().stream()
					.map(InvoiceLine::getTrack)
					.map(Track::getAlbum)
					.map(Album::getArtist)
					.toList();
			
			List<Artist> artists = new ArrayList<>(extractedArtits);
			
			if(artistMap.containsKey(year)) {
				artistMap.get(year).addAll(artists);
			} else {
				artistMap.put(year, artists);
			}
		});
		
		Map<Integer, StatisticsDTO> statisticMap = buildRawArtistStatistics(artistMap);
		statisticMap.entrySet().forEach(c -> c.getValue().setBestYearCount());
		
		return new ArrayList<>(statisticMap.values());
	}

	private Map<Integer, StatisticsDTO> buildRawArtistStatistics(Map<Integer, List<Artist>> artistMap) {
		Map<Integer, StatisticsDTO> statisticMap = new TreeMap<>();
		artistMap.forEach((year, artistList) -> {
			Map<Integer, List<Artist>> subArtistMap = artistList.stream().collect(Collectors.groupingBy(Artist::getArtistId));
			
			subArtistMap.forEach((artistId, subArtistList) -> {
				if(statisticMap.containsKey(artistId)) {
					StatisticsDTO dto = statisticMap.get(artistId);
					
					if(dto.getYearTrackCountSold().containsKey(year)) {
						dto.getYearTrackCountSold().put(year, 
								dto.getYearTrackCountSold().get(year) + subArtistList.size());
					} else {
						dto.getYearTrackCountSold().put(year, subArtistList.size());
					}
					
				} else {
					StatisticsDTO dto = new StatisticsDTO();
					dto.setArtistsDTO(ArtistMapper.toDto(subArtistList.get(0)));
					dto.setYearTrackCountSold(new TreeMap<>());
					dto.getYearTrackCountSold().put(year, subArtistList.size());
					
					statisticMap.put(artistId, dto);
				}
			});
			
		});
		
		return statisticMap;
	}

}
