/**
 * 
 */
package com.pouvoir.judiciaire.dsi.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pouvoir.judiciaire.dsi.dto.AlbumDTO;
import com.pouvoir.judiciaire.dsi.dto.ArtistDTO;
import com.pouvoir.judiciaire.dsi.entities.Album;
import com.pouvoir.judiciaire.dsi.entities.Artist;
import com.pouvoir.judiciaire.dsi.entities.Track;
import com.pouvoir.judiciaire.dsi.mapper.AlbumMapper;
import com.pouvoir.judiciaire.dsi.mapper.ArtistMapper;
import com.pouvoir.judiciaire.dsi.repository.ArtistRepository;
import com.pouvoir.judiciaire.dsi.repository.TrackRepository;

/**
 *  @author Véronque DIBI
 */

@Service
public class ArtistService {

	
	private final ArtistRepository artistRepository;
	
	private final TrackRepository trackRepository;
	
	
	public ArtistService(ArtistRepository artistRepository, TrackRepository trackRepository) {
        this.artistRepository = artistRepository;
        this.trackRepository = trackRepository;
    }
	

	@Transactional
	public ArtistDTO getArtistById(Integer artistId) {
		Optional<Artist> res = artistRepository.findById(artistId);
		Artist artist = res.orElse(null);
		return ArtistMapper.toDto(artist);
	}
	
	@Transactional
	public List<ArtistDTO> getAllArtists(){
		List<Artist> artists = artistRepository.findAll();
		return artists.stream().map(ArtistMapper::toDto).toList();
	}

	@Transactional
	public List<ArtistDTO> getAllArtistsAndAlbums(){
		List<Track> tracks = this.trackRepository.findAllTracksForAlbums();
		Map<Album, List<Track>> albumMap = tracks.stream().collect(Collectors.groupingBy(Track::getAlbum));
		
		List<ArtistDTO> result = handleTracksAndAlbums(albumMap);
		
		return result;
		
	}


	@Transactional(readOnly=true)
	public ArtistDTO getArtistAndAlbumsByArtistId(Integer artistId) {
		List<Track> tracks = this.trackRepository.findAllTracksForAlbumsByArtistId(artistId);
		Map<Album, List<Track>> albumMap = tracks.stream().collect(Collectors.groupingBy(Track::getAlbum));
		
		List<ArtistDTO> result = handleTracksAndAlbums(albumMap);
		
		return result.get(0);
	}


	private List<ArtistDTO> handleTracksAndAlbums(Map<Album, List<Track>> albumMap) {
		Map<Album, AlbumDTO> albumDTOMap = new HashMap<>();
		albumMap.forEach((album, trackList) -> {
			albumDTOMap.put(album,  AlbumMapper.toDto(album, trackList));
		});
		
		
		Map<Artist, List<Album>> artistMap = albumMap.keySet().stream().collect(Collectors.groupingBy(Album::getArtist));
		
		List<ArtistDTO> result = new ArrayList<>();
		artistMap.forEach((artist, albums) -> {
			List<AlbumDTO> albumsDTO = albums.stream().map(album -> albumDTOMap.get(album)).toList();
			ArtistDTO artistDTO = ArtistMapper.toDto(artist);
			artistDTO.setAlbums(albumsDTO);
			result.add(artistDTO);
		});
		return result;
	}

}
