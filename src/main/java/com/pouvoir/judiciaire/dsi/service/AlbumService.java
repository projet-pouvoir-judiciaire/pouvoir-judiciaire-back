/**
 * 
 */
package com.pouvoir.judiciaire.dsi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pouvoir.judiciaire.dsi.entities.Album;
import com.pouvoir.judiciaire.dsi.repository.AlbumRepository;

/**
 * @author Véronque DIBI
 */

@Service
public class AlbumService {

	
	private AlbumRepository albumRepository;
	
	@Autowired
	public AlbumService(AlbumRepository albumRepository) {
        this.albumRepository = albumRepository;
    }
	
	
	@Transactional
	public Album getAlbumById(Integer albumId) {
		Optional<Album> res = albumRepository.findById(albumId);
		return res.orElse(null);
		
	}

	@Transactional
	public List<Album> getAlbum(){
		return this.albumRepository.findAll();
		
	}
}
