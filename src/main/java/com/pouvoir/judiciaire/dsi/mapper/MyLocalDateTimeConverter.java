package com.pouvoir.judiciaire.dsi.mapper;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import jakarta.persistence.AttributeConverter;

/**
 * @author Veronique DIBI
 */
public class MyLocalDateTimeConverter implements AttributeConverter<LocalDateTime, String> {

	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	@Override
	public String convertToDatabaseColumn(LocalDateTime date) {
		if(date == null) {
			return null;
		} else {
			//yyyy-MM-dd HH:MI:ss
			return DATE_FORMATTER.format(date);
		}
	}

	@Override
	public LocalDateTime convertToEntityAttribute(String date) {
		if(date == null) {
			return null;
		} else {
			//yyyy-MM-dd HH:mm:ss
			return LocalDateTime.parse(date, DATE_FORMATTER);
		}
	}
}