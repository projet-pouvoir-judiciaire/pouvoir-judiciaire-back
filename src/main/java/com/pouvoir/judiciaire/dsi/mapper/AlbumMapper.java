package com.pouvoir.judiciaire.dsi.mapper;

import java.util.List;

import com.pouvoir.judiciaire.dsi.dto.AlbumDTO;
import com.pouvoir.judiciaire.dsi.entities.Album;
import com.pouvoir.judiciaire.dsi.entities.Track;
/**
 * @author Veronique DIBI
 */
public class AlbumMapper {

	public AlbumMapper() {
		// TODO Auto-generated constructor stub
	}
	
	public static AlbumDTO toDto(Album album, List<Track> tracks) {
		if(album == null) {
			return null;
		}
		AlbumDTO dto = new AlbumDTO();
		dto.setTitle(album.getTitle());
		dto.setGender(tracks.get(0).getGenre().getName());
		long totalLength = tracks.stream().map(Track::getMilliseconds).reduce(0, (a, b) -> a + b);
		dto.setTotalLengthInSeconds(totalLength / 1000);
		
		return dto;
	}

}
