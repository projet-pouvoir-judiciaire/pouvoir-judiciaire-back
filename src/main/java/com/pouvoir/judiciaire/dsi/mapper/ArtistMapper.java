package com.pouvoir.judiciaire.dsi.mapper;

import com.pouvoir.judiciaire.dsi.dto.ArtistDTO;
import com.pouvoir.judiciaire.dsi.entities.Artist;

/**
 * @author Veronique DIBI
 */
public class ArtistMapper {

	public ArtistMapper() {
	}
	
	public static ArtistDTO toDto(Artist artist) {
		if(artist == null) {
			return null;
		}
		ArtistDTO dto = new ArtistDTO();
		dto.setArtistId(artist.getArtistId());
		dto.setName(artist.getName());
		
		return dto;
	}

}
